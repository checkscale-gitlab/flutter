FROM universumstudios/android:sdk

### CONFIGURATION ==================================================================================

# Environment variables:
ENV FLUTTER_CHANNEL=stable
ENV FLUTTER_HOME="/flutter/sdk"
ENV FLUTTER_ROOT=$FLUTTER_HOME

# Installation arguments:
ARG FLUTTER_SDK_URL="https://github.com/flutter/flutter.git"

### IMAGE DEPENDENCIES & TOOLS =====================================================================

# Install LCOV coverage tool.
RUN apt-get -q -y update && apt-get -y install lcov
	
### FLUTTER ========================================================================================

# Download SDK tools:
RUN set -o errexit -o nounset && \
    echo "Creating Flutter HOME directory" && \
    mkdir -p $FLUTTER_HOME && \
    \
    echo "Downloading Flutter SDK tools" && \
    git clone $FLUTTER_SDK_URL $FLUTTER_HOME

# Export environment variables for Flutter tools specific directories.
ENV PATH="$PATH:${FLUTTER_HOME}/bin:${FLUTTER_HOME}/bin/cache/dart-sdk/bin"

# Switch to desired channel:
RUN flutter channel ${FLUTTER_CHANNEL}

# Update SDK components:
RUN set -o errexit -o nounset && \
    echo "Updating Flutter SDK components" && \
    flutter upgrade && \
    \
    echo "Accepting Android SDK licenses" && \
    echo yes | sdkmanager --licenses
    #echo y | flutter doctor --android-licenses

# Set working directory:
WORKDIR /flutter

### VERIFICATION ===================================================================================

RUN set -o errexit -o nounset && \
    echo "Testing Flutter SDK configuration" && \
    flutter doctor -v